# Community

Fostering a vibrant cross-Defence DevSecOps Community whose user needs drive the development of the Foundry DevSecOps service is a priority for the project. The project is developing the following community engagement offerings to support this aim.   

![Community Icons](https://glassumbrellapublicbucket.s3.eu-central-1.amazonaws.com/Community+Icons.png)
## Confluence Site
*Launching December 2021*

- The latest information about the PREDA project
- Latest project team blog content
- Key contacts and access

## Dojo Experience
*Under Construction*

- Immersive learning experiences to support development teams in using the Foundry DevSecOps service
- Supported by PREDA developers and engineers
- Hands-on learning

## DevSecOps Community of Practice

- Fortnightly community call where development teams and sponsors from across Defence share their latest project news
- Latest project delivery updates
- Open invitation

## PREDA Alliance
*Launching January 2022*

- Experienced development teams supporting the strategic direction of the PREDA project
- Early adopters of the Foundry DevSecOps Service

## Developer Portal
*Under Construction*

- Documentation and user guide content
- Connection to the Foundry DevSecOps platform and toolkit
- Source code repo
- Links to training content
- Community events and forum links
