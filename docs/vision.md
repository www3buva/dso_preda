# PREDA Vision, Mission, Intent

## Our Vision

To enable Defence to prevent or win in conflict using software created and fine tuned for the challenges of the moment and needs of the user.

## Our Mission

To lay the engineering foundations for truly digital services to be brought the frontline and to wider Defence. To identify and exploit opportunities related to people, process, technology and information that will improve and accelerate the development & deployment of software. To serve all those who provide software for Defence, across our enterprise and our industry partners.

## PREDA Intent
### A User-Centered Service
- To be driven by a deep understanding of the user communities across Defence.
- To provide service offerings that can be flexed and extended to meet the needs of individual teams.
- To actively seek, learn and adapt from user feedback as the Foundry DSO service evolves.
- To design with data and provide excellent user experience.

### Rapid Software Delivery
- To accelerate Defence’s ability to deliver production grade software into the hands of users, particularly in battlespace: achieve ‘compile to combat’ in under 24 hours
- To harness Cloud Native Application Platform technologies to create modern applications that are fast to deploy and easy to update
- Common standards, patterns and procedures are key to both acceleration and sustainability: apps should be ‘cattle’ not ‘pets’. We will drive the adoption of standards across the Defence estate.

### A Step-Change in Digital Capability for Defence
- To enable truly user centric, digital services to be produced in line with the Foundry Way.
- To harness best practice DSO methods and Agile manifesto principles
- To build Cross-Defence digital skills and expertise (SQEP)
- To support sharing and reuse of assets across Defence commands and teams
- To adopt, enable and promote ‘Secure by Design’ technology and innovation

### Accelerated Time to Value
- To provide the ‘go to’ services of choice for secure, rapid software development across the OFFICIAL and SECRET classifications allowing teams to start delivering value quickly.
- To deliver speed and cost effectiveness for Defence by providing platforms, allowing service teams to focus on valuable features (rather than technical enablers)
- Normalise practices of delivering small batches of valuable features to the frontline and wider enterprise.
